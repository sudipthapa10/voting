package com.voting.voting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.voting.voting.model.Fruits;
import com.voting.voting.model.Student;
import com.voting.voting.repo.FruitRepo;


@RestController
public class FruitController {
	@Autowired
	FruitRepo repo;
   
	
	@RequestMapping(value = "/api/fruit", method = RequestMethod.GET)
	public List<Fruits> getFruitWithVotes() {
		return repo.getFruits();
	}
	
	
	
	
}
