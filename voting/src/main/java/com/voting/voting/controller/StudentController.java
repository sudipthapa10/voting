package com.voting.voting.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.voting.voting.model.Student;
import com.voting.voting.repo.StudentRepo;


@RestController
public class StudentController {
	
	@Autowired
	StudentRepo repo;
	
	@RequestMapping(value = "/api/student/{id}", method = RequestMethod.GET)
	public Student getUserVote(@PathVariable int id) {
		return repo.getStudentInfo(id);
	}
	
	@RequestMapping(value = "/api/student", method = RequestMethod.POST)
	public Student  addStudent(@RequestBody Student student) {
		return repo.addStudent(student);
	}

}
