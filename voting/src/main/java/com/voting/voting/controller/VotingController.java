package com.voting.voting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.voting.voting.model.Vote;
import com.voting.voting.repo.VoteRepo;

@RestController
public class VotingController {
	
	@Autowired
	VoteRepo repo;
	
	@RequestMapping(value = "/api/vote", method = RequestMethod.POST)
	public void vote(@RequestBody Vote vote) {
         repo.vote(vote.getFruitId(), vote.getStudentId());

	}

}
