package com.voting.voting.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.voting.voting.model.Fruits;

public class FruitMapper implements RowMapper<Fruits> {

	@Override
	public Fruits mapRow(ResultSet rs, int row) throws SQLException {
		Fruits fruit = new Fruits();
		fruit.setName(rs.getString("fruit_name"));
		fruit.setVote(rs.getInt("total_votes"));
		fruit.setId(rs.getInt("fruit_id"));
		return fruit;

	}

}
