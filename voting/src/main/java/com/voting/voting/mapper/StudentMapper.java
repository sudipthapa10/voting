package com.voting.voting.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.voting.voting.model.Student;

public class StudentMapper implements RowMapper {
	
	@Override
	public Student mapRow(ResultSet rs, int row) throws SQLException {
		Student student = new Student();
		student.setName(rs.getString("name"));
		student.setId(rs.getInt("id"));
		student.setVotedFruitId(rs.getInt("fruit_id"));
		return student;
	}

}
