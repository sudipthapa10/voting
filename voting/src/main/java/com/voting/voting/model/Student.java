package com.voting.voting.model;


public class Student {
	
	private int id ;
	
	private String name;
	
	private int votedFruitId;
	
	

	public int getVotedFruitId() {
		return votedFruitId;
	}

	public void setVotedFruitId(int votedFruitId) {
		this.votedFruitId = votedFruitId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	

}
