package com.voting.voting.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.voting.voting.mapper.FruitMapper;
import com.voting.voting.model.Fruits;

@Repository
public class FruitRepo {
	 @Autowired
	    JdbcTemplate jdbcTemplate;
	 
	 public List<Fruits> getFruits() {
		 return jdbcTemplate.query("select  fr.fruit_name ,aggs.fruit_id, aggs.total_votes " + 
		 		"from " + 
		 		"(select   f.fruit_id, count(v.fruit_id) as total_votes " + 
		 		" from " + 
		 		"voitngapp.dbo.fruit f left join voitngapp.dbo.votes v on (f.FRUIT_ID = v.fruit_id) " + 
		 		"group by f.fruit_id) as aggs " + 
		 		" inner join  voitngapp.dbo.fruit  fr on ( fr.fruit_id = aggs.fruit_id)"
		 		+ " ", new FruitMapper());
		 
	 }
	 

	 

	 
	 
	 
	 
	 
	 
	
	
}