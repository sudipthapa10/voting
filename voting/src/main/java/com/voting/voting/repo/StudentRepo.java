package com.voting.voting.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import com.voting.voting.mapper.StudentMapper;
import com.voting.voting.model.Student;

@Repository
public class StudentRepo {
	
	 @Autowired
	    JdbcTemplate jdbcTemplate;
	 
	 public Student addStudent(Student student) {
		    final String sql = "insert into voitngapp.dbo.student(id,name) values(?,?)";
		    
	        jdbcTemplate.update(new PreparedStatementCreator() {
	            @Override
	            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	                PreparedStatement ps = connection.prepareStatement(sql);
	                ps.setInt(1, student.getId());
	                ps.setString(2, student.getName());
	                return ps;
	            }
	        });

	        return student;
		 
		 
	 }
	 
	 public Student getStudentInfo(int id) {
		 String query = "select s.name , s.id , v.fruit_id from voitngapp.dbo.votes v "
				 + "inner join voitngapp.dbo.student as s on (v.student_id = s.id) "
				 + " where s.ID =? ";
		List <Student> student = jdbcTemplate.query(query, new Object[] { id },
	             new StudentMapper());
		return student.get(0);
		
	
				 
				
		 
		 

		 
	 }

}
