package com.voting.voting.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class VoteRepo {
	
	@Autowired
	JdbcTemplate template;
	
	 public void vote(int fruitId, int studentId) {
		 final String sql = "insert into voitngapp.dbo.votes(fruit_id,student_id) values(?,?)";
		 
	        KeyHolder holder = new GeneratedKeyHolder();
	        template.update(new PreparedStatementCreator() {
	            @Override
	            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
	                PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
	                ps.setInt(1, fruitId);
	                ps.setInt(2, studentId);
	                return ps;
	            }
	        }, holder);
	 
	        int votersId = holder.getKey().intValue();
	        System.out.println(votersId);
	    
		 
	 }

}
